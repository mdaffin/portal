FROM ubuntu:17.10
MAINTAINER Michael Daffin

RUN apt update -y && \
    apt install -y qemu qemu-user-static binfmt-support parted wget dosfstools \
    zip gcc-arm-linux-gnueabihf libc6-armhf-cross libc6-dev-armhf-cross

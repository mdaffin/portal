# portal
A hackerspace two way portal called 'portal' because naming things is hard.

## Building the image with docker

```
docker build . -t image-builder
docker run --privileged --rm -it -v $PWD:/code -w /code image-builder ./create-image
```
